package ru.mts.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.mts.data.Colors;

@RestController
public class ThemeRestController {

    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/colors")
    public Colors colors() {
        return new Colors(counter.incrementAndGet());
    }
}