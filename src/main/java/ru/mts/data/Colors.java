package ru.mts.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Random;

public class Colors {

    public Colors(long id) {
        this.id = id;
    }

    private final long id;
    private final Random random = new Random();

    @JsonProperty("font_color")
    private String fontColor;

    @JsonProperty("background_color")
    private String backgroundColor;

    private String generateColor() {
        int nextInt = random.nextInt(0xffffff + 1);
        return String.format("#%06X", nextInt);
    }

    public long getId() {
        return id;
    }

    public String getFontColor() {
        return generateColor();
    }

    public String getBackgroundColor() {
        return generateColor();
    }
}
